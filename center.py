province_centers = {
    "กรุงเทพมหานคร": [13.7563, 100.5018],
    "กระบี่": [8.0863, 98.9063],
    "กาญจนบุรี": [14.0228, 99.5327],
    "กาฬสินธุ์": [16.4322, 103.5061],
    "กำแพงเพชร": [16.4827, 99.5229],
    "ขอนแก่น": [16.432, 102.823],
    "จันทบุรี": [12.6117, 102.103],
    "ฉะเชิงเทรา": [13.6826, 101.0792],
    "ชลบุรี": [13.3611, 100.9847],
    "ชัยนาท": [15.1989, 100.116],
    "ชัยภูมิ": [15.804, 102.038],
    "ชุมพร": [10.4931, 99.1802],
    "เชียงราย": [19.9072, 99.8308],
    "เชียงใหม่": [18.7061, 98.9817],
    "ตรัง": [7.6167, 99.9500],
    "ตราด": [12.2449, 102.5167],
    "ตาก": [16.8847, 99.1297],
    "นครนายก": [14.2061, 101.215],
    "นครปฐม": [13.8196, 100.0443],
    "นครพนม": [17.3089, 104.1601],
    "นครราชสีมา": [14.9799, 102.0976],
    "นครศรีธรรมราช": [8.4306, 99.9645],
    "นครสวรรค์": [15.6981, 100.1255],
    "นนทบุรี": [13.8592, 100.521],
    "นราธิวาส": [6.4271, 101.8233],
    "น่าน": [18.7904, 100.7814],
    "บึงกาฬ": [18.3612, 103.6505],
    "บุรีรัมย์": [14.9938, 103.102],
    "ปทุมธานี": [14.0228, 100.5281],
    "ประจวบคีรีขันธ์": [11.8091, 99.7983],
    "ปราจีนบุรี": [14.0583, 101.373],
    "ปัตตานี": [6.8746, 101.2541],
    "พระนครศรีอยุธยา": [14.353, 100.552],
    "พังงา": [8.4501, 98.5307],
    "พัทลุง": [7.6167, 100.0667],
    "พิจิตร": [16.2073, 100.4068],
    "พิษณุโลก": [16.8248, 100.2564],
    "เพชรบุรี": [13.0975, 99.950],
    "เพชรบูรณ์": [16.4173, 101.1595],
    "แพร่": [18.1457, 100.1626],
    "พะเยา": [19.2166, 99.8825],
    "ภูเก็ต": [7.8804, 98.3923],
    "มหาสารคาม": [16.0131, 103.0907],
    "มุกดาหาร": [16.5453, 104.6467],
    "แม่ฮ่องสอน": [19.3003, 97.9686],
    "ยโสธร": [15.7941, 104.1453],
    "ยะลา": [6.5394, 101.2814],
    "ร้อยเอ็ด": [16.0587, 103.6529],
    "ระนอง": [12.2251, 99.8638],
    "ระยอง": [12.6767, 101.2578],
    "ราชบุรี": [13.528, 99.8135],
    "ลพบุรี": [14.7995, 100.6536],
    "ลำปาง": [18.2871, 99.4926],
    "ลำพูน": [18.5743, 99.0086],
    "เลย": [17.4887, 101.7224],
    "ศรีสะเกษ": [15.1181, 104.322],
    "สกลนคร": [17.1463, 104.142],
    "สงขลา": [7.1858, 100.601],
    "สตูล": [6.7033, 100.3726],
    "สมุทรปราการ": [13.5993, 100.596],
    "สมุทรสงคราม": [13.4146, 100.002],
    "สมุทรสาคร": [13.5479, 100.274],
    "สระแก้ว": [13.8198, 102.0646],
    "สระบุรี": [14.5271, 100.9101],
    "สิงห์บุรี": [14.891, 100.403],
    "สุโขทัย": [17.0056, 99.8268],
    "สุพรรณบุรี": [14.4746, 100.117],
    "สุราษฎร์ธานี": [9.1382, 99.3217],
    "สุรินทร์": [14.8823, 103.493],
    "หนองคาย": [17.8784, 102.7426],
    "หนองบัวลำภู": [17.599, 103.207],
    "อ่างทอง": [14.5898, 100.4553],
    "อำนาจเจริญ": [15.8589, 104.6282],
    "อุดรธานี": [17.4157, 102.785],
    "อุตรดิตถ์": [17.6287, 100.097],
    "อุทัยธานี": [15.3764, 100.0255],
    "อุบลราชธานี": [15.2384, 104.8475]
}