import dash
from dash import dcc, html, Input, Output
import plotly.express as px
import pandas as pd
import json
import requests
from center import province_centers

app = dash.Dash(__name__)

geojson_url = "https://raw.githubusercontent.com/chingchai/OpenGISData-Thailand/master/provinces.geojson"
response = requests.get(geojson_url)
data_geojson = response.json()

json_url = "https://gpa.obec.go.th/reportdata/pp3-4_2566_province.json"
response = requests.get(json_url)
data_json = response.json()

province_dict = {feature["properties"]["pro_th"]: feature for feature in data_geojson["features"]}

for province_data in data_json:
    province_name = province_data["schools_province"]
    if province_name in province_dict:
        province_dict[province_name]["properties"].update(province_data)

df_geojson = pd.json_normalize(data_geojson['features'])
df_geojson['pro_th'] = df_geojson['properties.pro_th']

df_csv = pd.read_csv('convert.csv')

df_csv['Latitude'] = df_csv['schools_province'].map(lambda x: province_centers.get(x, [None, None])[0])
df_csv['Longitude'] = df_csv['schools_province'].map(lambda x: province_centers.get(x, [None, None])[1])

df = pd.merge(df_csv, df_geojson, left_on='schools_province', right_on='pro_th', how='left')

dropdown_options = [{'label': name, 'value': name} for name in df['schools_province'].unique()]

totalstd = df['totalstd'].sum()
totalmale = df['totalmale'].sum()
totalfemale = df['totalfemale'].sum()

# สร้าง layout ของแอป
app.layout = html.Div(style={'display': 'flex', 'flexDirection': 'column', 'alignItems': 'left', 'height': '90vh', 'font-family': 'Comic sans MS'}, children=[
    html.Div(style={'textAlign': 'left', 'margin-bottom': '8px', 'font-family': 'Comic sans MS', 'border': '2px solid #000000', 'border-radius': '10px', 'width': '100%'}, children=[
    html.Img(src="https://www.reshot.com/preview-assets/icons/GQFMY4NAUV/graduation-GQFMY4NAUV.svg", style={'height': '45px', 'margin-left': '8px', 'display': 'inline-block', 'vertical-align': 'middle'}),
    html.H2(children='Graduation Student Dashboard', style={'color': '#2c2385', 'padding': '5px', 'margin': '0', 'font-weight': 'normal', 'display': 'inline-block', 'vertical-align': 'middle', 'margin-left': '10px'}),
    ]),
    html.Div(style={'display': 'flex', 'width': '100%', 'height': '96%'}, children=[
        html.Div(style={'width': '20%', 'padding': '8px','border': '2px solid #000000','border-radius': '6px', 'margin-right': '8px', 'overflowY': 'scroll', 'height': '90vh', 'color': 'black','font-size': '0.8em'}, children=[
            dcc.Dropdown(
                id='province-dropdown',
                options=dropdown_options,
                style = {'border': '1px solid #000000','border-radius': '5px'},
                placeholder='Select provinces',
                multi=True  # Add this line to enable multiple selections
            ),
            html.Div(id='province-info', children=[], style={'padding': '8px', 'margin': '0', 'height': '97%'})  # เพิ่ม ID เพื่อเชื่อมโยง callback
        ]),
        html.Div(style={'width': '65%', 'height': '95%', 'display': 'flex', 'flexDirection': 'column'}, children=[
            html.Div(style={'flex': '1','border': '2px solid #000000','border-radius': '6px', 'margin-bottom': '8px', 'height': '65vh'}, children=[
                dcc.Graph(id='map', style={'height': '65vh'})
            ]),
            html.Div(id='bar-chart-container', style={'height': '30vh','border': '2px solid #000000','border-radius': '6px', 'backgroundColor': '#f9f9f9', 'font-family': 'Comic sans MS'}),
        ]),
        html.Div(style={'width': '15%', 'flexDirection': 'column', 'justifyContent': 'space-around'}, children=[
            html.Div([
                html.Div([
                    html.H4("All Student", style={'color': '#000000', 'font-family': 'Comic sans MS', 'font-size': '1.3em', 'margin-bottom': '20px'}),
                    html.Div([
                        html.Img(src="https://www.reshot.com/preview-assets/icons/GQFMY4NAUV/graduation-GQFMY4NAUV.svg", style={'height': '70px', 'margin-right': '15px'}),
                    html.H2(totalstd, style={'font-size': '0.9em', 'color': 'black', 'font-family': 'Comic sans MS', 'font-size': '1.4em'})
                    ], style={'display': 'flex', 'flexDirection': 'row', 'alignItems': 'center'})
                ], style={'display': 'flex', 'flexDirection': 'column', 'alignItems': 'center', 'textAlign': 'center', 'flex': '1'})
            ], style={'border': '2px solid #000000','border-radius': '10px', 'backgroundColor': '#c6f2f7', 'padding': '10px', 'height': '30%', 'marginTop': '15px', 'marginBottom': '28px', 'marginLeft': '8px', 'display': 'flex', 'alignItems': 'center'}),
            html.Div([
                html.Div([
                    html.H4("All Male Student", style={'color': '#000000', 'font-family': 'Comic sans MS', 'font-size': '1.3em', 'margin-bottom': '20px'}),
                    html.Div([
                        html.Img(src="https://www.reshot.com/preview-assets/icons/UZHB25MWAK/male-graduated-student-UZHB25MWAK.svg", style={'height': '70px', 'margin-right': '15px'}),
                    html.H2(totalmale, style={'font-size': '0.9em', 'color': 'black', 'font-family': 'Comic sans MS', 'font-size': '1.4em'})
                    ], style={'display': 'flex', 'flexDirection': 'row', 'alignItems': 'center'})
                ], style={'display': 'flex', 'flexDirection': 'column', 'alignItems': 'center', 'textAlign': 'center', 'flex': '1'})
            ], style={'border': '2px solid #000000','border-radius': '10px', 'backgroundColor': '#e36286', 'padding': '10px', 'height': '30%','marginBottom': '25px', 'marginLeft': '8px', 'display': 'flex', 'alignItems': 'center'}),
            html.Div([
                html.Div([
                    html.H4("All Female Student", style={'color': '#000000', 'font-family': 'Comic sans MS', 'font-size': '1.3em', 'margin-bottom': '20px'}),
                    html.Div([
                        html.Img(src="https://www.reshot.com/preview-assets/icons/E463MDGPH2/female-graduated-student-E463MDGPH2.svg", style={'height': '70px', 'margin-right': '15px'}),
                    html.H2(totalfemale, style={'font-size': '0.9em', 'color': 'black', 'font-family': 'Comic sans MS', 'font-size': '1.4em'})
                    ], style={'display': 'flex', 'flexDirection': 'row', 'alignItems': 'center'})
                ], style={'display': 'flex', 'flexDirection': 'column', 'alignItems': 'center', 'textAlign': 'center', 'flex': '1'})
            ], style={'border': '2px solid #000000','border-radius': '10px', 'backgroundColor': '#e396ab', 'padding': '10px', 'height': '30%', 'marginLeft': '8px', 'display': 'flex', 'alignItems': 'center'}),
        ])
    ])
])

@app.callback(
    Output('province-info', 'children'),
    Input('province-dropdown', 'value')
)
def update_province_info(selected_provinces):
    if not selected_provinces:
        selected_provinces = df['schools_province'].unique()

    info_boxes = []
    for province_name in selected_provinces:
        province_data = df[df['schools_province'] == province_name].iloc[0]
        info_box = html.Div([
            html.H3(province_name, style={'color': '#5D78C8', 'font-size': '1.2em', 'margin-bottom': '0.4em'}),
            html.P(f"• Total Students: {province_data['totalstd']}", style={'color': 'black', 'margin-bottom': '0.2em', 'margin-left': '1em'}),
            html.P(f"• Total Males: {province_data['totalmale']}", style={'color': 'black', 'margin-bottom': '0.2em', 'margin-left': '1em'}),
            html.P(f"• Total Females: {province_data['totalfemale']}", style={'color': 'black', 'margin-bottom': '0.2em', 'margin-left': '1em'}),
        ], style={'padding': '8px', 'border': '1px solid #ddd', 'backgroundColor': '#f9f9f9', 'font-size': '0.8em'})
        info_boxes.append(info_box)

    return info_boxes

@app.callback(
    Output('map', 'figure'),
    Input('province-dropdown', 'value')
)
def update_map(selected_provinces):
    if not selected_provinces:
        selected_provinces = df['schools_province'].unique()
    
    filtered_df = df[df['schools_province'].isin(selected_provinces)]
    lats = [province_centers[province][0] for province in selected_provinces if province in province_centers and province_centers[province][0] is not None]
    lons = [province_centers[province][1] for province in selected_provinces if province in province_centers and province_centers[province][1] is not None]
    center_lat = sum(lats) / len(lats)
    center_lon = sum(lons) / len(lons)
    zoom = 6 if len(selected_provinces) == 1 else 5 if len(selected_provinces) <= 3 else 4
    
    fig = px.choropleth_mapbox(
        filtered_df,
        geojson=data_geojson,
        locations='schools_province',
        featureidkey="properties.pro_th",
        color='properties.pro_en',
        hover_data={
            'schools_province': True,
            'totalstd': True,
            'totalfemale': True,
            'totalmale': True
        },
        mapbox_style="open-street-map",
        zoom=zoom,
        center={"lat": center_lat, "lon": center_lon},
        opacity=0.7
    )

    fig.update_layout(
        margin={"r": 0, "t": 0, "l": 0, "b": 0},  # กำหนด margin เพื่อลดพื้นที่ว่าง
        mapbox=dict(
            style="open-street-map",
            zoom=zoom,
            center={"lat": center_lat, "lon": center_lon}
        )
    )

    return fig

@app.callback(
    Output('bar-chart-container', 'children'),
    Input('province-dropdown', 'value')
)
def update_bar_chart(selected_provinces):
    if not selected_provinces:
        selected_provinces = df['schools_province'].unique()
    
    filtered_df = df[df['schools_province'].isin(selected_provinces)]
    
    bar_chart_fig = px.bar(
        filtered_df,
        x='schools_province',
        y=['totalmale', 'totalfemale'],
        title='Total Male and Female Students by Province',
        barmode='group',
        color_discrete_sequence=['#a9efaf', '#0bd51d'],  # กำหนดสีของแท่งกราฟ
    )

    bar_chart_fig.update_layout(
        plot_bgcolor='#f9f9f9',
        paper_bgcolor='#f9f9f9',
        margin=dict(l=0, r=0, t=35, b=0),
        height=245,  # ลดขนาดของ Bar Chart
        font=dict(size=10, family='Comic sans MS'),  # กำหนดฟอนต์และขนาดตัวอักษร
        title_x=0.46, # กำหนด title ให้ตรงกลาง
    )

    return dcc.Graph(figure=bar_chart_fig)

if __name__ == '__main__':
    app.run_server(debug=True)
